from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(max_length=24, widget=forms.TextInput())
    password = forms.CharField(max_length=24, widget=forms.PasswordInput())

class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=24, widget=forms.TextInput())
    last_name = forms.CharField(max_length=24, widget=forms.TextInput())
    email = forms.EmailField(widget=forms.EmailInput())
    username = forms.CharField(max_length=24, widget=forms.TextInput())
    password = forms.CharField(max_length=24, widget=forms.PasswordInput())
    confirm_password = forms.CharField(max_length=24, widget=forms.PasswordInput())
    APPS = [
        ('parrasincapp', 'Parra INC'),
        ('researchescontainerapp', 'Researches container'),
    ]
    app = forms.ChoiceField(
        required=True,
        choices=APPS,
    )
