from django.urls import path
from examiiprojectauth import views

urlpatterns = [
    path('', views.index, name='auth_index'),
    path('signup/', views.user_signup, name='user_signup'),
    path('login/', views.user_login, name='user_login'),
    path('logout/', views.user_logout, name='user_logout')
]
