from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.utils.timezone import now

from parrasincapp.models import Employee
from researchescontainerapp.models import Autor

from .forms import LoginForm, SignupForm

# Create your views here.
def index(request):
    if request.user.is_authenticated:
        user_parrasincapp = None
        user_researchescontainerapp = None
        try:
            user_parrasincapp = Employee.objects.get(user=request.user)
            user_researchescontainerapp = Autor.objects.get(user=request.user)
        except:
            if user_researchescontainerapp != None:
                return redirect('http://localhost:8000/researchescontainerapp/')
            elif user_parrasincapp != None:
                return redirect('http://localhost:8000/parrasincapp')
    else:
        return redirect('user_login')

def user_signup(request):
    if request.user.is_authenticated:
        return redirect('auth_index')
    else:
        message = ''
        form = SignupForm()
        if request.method == 'POST':
            form = SignupForm(request.POST or None)
            password = request.POST['password']
            confirm_password = request.POST['confirm_password']
            if password == confirm_password:
                if form.is_valid():
                    user = User.objects.create_user(
                        username = request.POST['username'],
                        email = request.POST['email'],
                        first_name = request.POST['first_name'],
                        last_name = request.POST['last_name'],
                    )
                    user.set_password(password)

                    app = request.POST['app']
                    if app == 'parrasincapp':
                        employee = Employee.objects.create(
                            user = user,
                            slug = user.username,
                        )
                        employee.save()
                    elif app == 'researchescontainerapp':
                        autor = Autor.objects.create(
                            user = user,
                            slug = user.username,
                        )
                        autor.save()

                    user.save()
                    # login after signup
                    user = authenticate(request, username=request.POST['username'], password=password)
                    if user is not None:
                        if user.is_active:
                            login(request, user)
                            return redirect('auth_index')
                        else:
                            message = 'User is not active'
                    else:
                        message = 'User not signup correctly'

                else:
                    message='Invalid form'
            else:
                message='Passwords are not equals'
    context = {
        'form': form,
        'message': message
    }
    return render(request,'examiiprojectauth/signup.html',context)


def user_login(request):
    if request.user.is_authenticated:
        return redirect('auth_index')
    else:
        message = 'Not logged'
        form = LoginForm()
        if request.method == 'POST':
            form = LoginForm(request.POST or None)
            if form.is_valid():
                username = request.POST['username']
                password = request.POST['password']
                user = authenticate(request, username=username, password=password)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        return redirect('auth_index')
                    else:
                        message = 'User is not active'
                else:
                    message = 'Username or password is not correct'
            else:
                message='Invalid form'
        context = {
            'form': form,
            'message': message
        }
        return render(request,'examiiprojectauth/login.html',context)

def user_logout(request):
    logout(request)
    return redirect('user_login')
