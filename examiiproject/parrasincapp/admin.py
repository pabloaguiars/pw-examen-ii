from django.contrib import admin
from .models import Employee
from .models import Software
from .models import SoftwareFunction
from .models import Department

# Register your models here.
admin.site.register(Employee)
admin.site.register(Software)
admin.site.register(SoftwareFunction)
admin.site.register(Department)
