from django.apps import AppConfig


class ParrasincappConfig(AppConfig):
    name = 'parrasincapp'
