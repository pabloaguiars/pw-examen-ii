from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import now

# Create your models here.
class Department(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=144)
    slug = models.SlugField(max_length=255,default=now)

    def __str__(self):
        return self.name

class SoftwareFunction(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=144)
    slug = models.SlugField(max_length=255,default=now)

    def __str__(self):
        return 'Name: %s Description: %s' % (self.name, self.description)

class Employee(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True)
    slug = models.SlugField(max_length=255)

    def __str__(self):
        return self.slug

class Software(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=144)
    departament = models.ForeignKey(Department, on_delete=models.CASCADE)
    software_function = models.ManyToManyField(SoftwareFunction)
    slug = models.SlugField(max_length=255,default=now)

    def __str__(self):
        return self.name
