from django.urls import path
from parrasincapp import views

urlpatterns = [
    path('', views.index, name='parrasincapp_index'),
    path('software/create', views.SoftwareCreate.as_view(), name='software_create'),
    path('software/function/create', views.SoftwareFunctionCreate.as_view(), name='software_function_create'),
    path('department/create', views.DepartmentCreate.as_view(), name='deparment_create'),
    path('directory/', views.DirectoryList.as_view(), name='directory'),
    path('search/', views.List.as_view(), name="search"),
]
