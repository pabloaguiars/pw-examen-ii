from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.db.models import Q
from .models import Software, SoftwareFunction, Department

# Create your views here.
def index(request):
    return render(request,'parrasincapp/index.html',{})

class SoftwareCreate(CreateView):
    model = Software
    fields = [
        'name',
        'description',
        'departament',
        'software_function'
    ]
    template_name = 'parrasincapp/software-create.html'
    success_url = reverse_lazy('parrasincapp_index')

class SoftwareFunctionCreate(CreateView):
    model = SoftwareFunction
    fields = [
        'name',
        'description'
    ]
    template_name = 'parrasincapp/software-function-create.html'
    success_url = reverse_lazy('parrasincapp_index')
    success_url = reverse_lazy('parrasincapp_index')

class DepartmentCreate(CreateView):
    model = Department
    fields = [
        'name',
        'description'
    ]
    template_name = 'parrasincapp/department-create.html'
    success_url = reverse_lazy('parrasincapp_index')

class DirectoryList(ListView):
    model = Software
    template_name = 'parrasincapp/directory.html'

class List(ListView):
    template_name = 'parrasincapp/search-result.html'

    def get_queryset(self, *args, **kwargs):
        qs = Software.objects.all()
        print(self.request)
        query = self.request.GET.get("q", None)
        print(query)
        if query is not None:
            qs = qs.filter(Q(name__icontains=query) | Q(departament__name__icontains=query) | Q(departament__id__icontains=query) | Q(software_function__name__icontains=query) | Q(software_function__description__icontains=query))
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(List, self).get_context_data(*args, **kwargs)
        return context
