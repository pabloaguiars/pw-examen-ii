from django.apps import AppConfig


class ResearchescontainerappConfig(AppConfig):
    name = 'researchescontainerapp'
