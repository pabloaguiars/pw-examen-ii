from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class AutorType(models.Model):
    name =  models.CharField(max_length=50)
    description = models.TextField(max_length=144)
    slug = models.SlugField(max_length=255)

class Category(models.Model):
    name =  models.CharField(max_length=50)
    description = models.TextField(max_length=144)
    slug = models.SlugField(max_length=255)

class Autor(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    author_type = models.ForeignKey(AutorType, on_delete=models.CASCADE,null=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE,null=True)
    slug = models.SlugField(max_length=255)

class Research(models.Model):
    doi = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    date = models.DateTimeField()
    autors = models.ManyToManyField(Autor)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    file = models.FileField(upload_to='files/%Y/%m/%d/')
    slug = models.SlugField(max_length=255)
