from django.urls import path
from researchescontainerapp import views

urlpatterns = [
    path('', views.index, name='researchescontainerapp_index'),
]
